# P-Bunker_generator scénario

Ce générateur est la base du projet Bunker, initialisé par Valkiri au étudiants d' Arts Numériques début Novembre 2020, en plein deuxième confinement.

Le générateur sert au sein du projet à proposer des scénarios de crises imaginaires qui vont servir comme base commune au productions et journal de bord des étudiants.